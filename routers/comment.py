from fastapi import APIRouter, Depends, status
from routers.schemas import PostDisplay, CommentBase
from sqlalchemy.orm.session import Session
from db.database import get_db
from routers.schemas import UserAuth
from auth.oauth2 import get_current_user
from db import db_comment

router = APIRouter(prefix="/comment", tags=["comment"])

@router.get('/all/{post_id}')
def comments(post_id: int, db: Session = Depends(get_db)):
    return db_comment.get_all(db, post_id)


@router.post('')
def create_comment(
    request: CommentBase,
    db: Session = Depends(get_db),
    current_user: UserAuth = Depends(get_current_user),
):
    return db_comment.create(db, request)
