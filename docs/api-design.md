### «Human-readable of the endpoint»


### Sign up

* Endpoint path: /user
* Endpoint method: POST

* Request shape (form):

  * username: string
  * email: string
  * password: string

* Response: True
* Response shape (JSON):
    ```json
    {
      "account": {
        «key»: type»,
      },
      "token": string
    }
    ```
