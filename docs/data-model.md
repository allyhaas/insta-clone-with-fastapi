# Data models
### users

| name             | type         | unique | optional |
| ---------------- | ------------ | ------ | -------- |
| id               | INTEGER      | yes    | no       |
| username         | STRING       | yes    | no       |
| email            | STRING       | yes    | no       |
| hashed_password  | STRING       | no     | no       |

The `users` table contains the data about a specific user.
